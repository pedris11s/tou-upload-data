from python_graphql_client import GraphqlClient
import os, json

# Instantiate the client with an endpoint.
client = GraphqlClient(endpoint="https://w232fcdkujajtdyn53zsw5vqce.appsync-api.us-west-2.amazonaws.com/graphql", headers={
    'x-api-key': 'da2-n45pbrywknfibbhhbvnaxe4kwy',
    'Content-Type': 'application/json'
})

# Create the query string and variables required for the request.
query = """
    mutation CreateClause($input: CreateClauseInput!){  
        CreateClause(input: $input){
            Code 
            Message
            Data {
                OwnerId
                ClauseKey
                TemplateS3Key
                FormUrl
                Created
                OwnerName
                ClauseName
                ClauseDescription
                Version
                Variables
            } 
        }
    }
"""
variables = {
    "input": { 
    }
}

file = open(os.path.join(os.path.dirname(__file__), "fixtures/clauses.json"), 'r', encoding="utf-8")
clauses_text = file.read()
clauses = json.loads(clauses_text)['clauses']

file = open(os.path.join(os.path.dirname(__file__), "fixtures/variables_clauses.json"), 'r', encoding="utf-8")
variables_clauses_text = file.read()
variables_clauses = json.loads(variables_clauses_text)['variables']

for clause in clauses:
    template_path = "templates/{}.html".format(clause['ClauseKey'])
    file = open(os.path.join(os.path.dirname(__file__), template_path), 'r', encoding="utf-8")
    template = file.read()
    clause['Template'] = template

    vars = next((obj for obj in variables_clauses if obj['clause_key'] == clause['ClauseKey']), None)
    clause['Variables'] = json.dumps(vars['data'])
    variables = {
        "input": clause
    }

    # Synchronous request
    data = client.execute(query=query, variables=variables)
    print("{} : {}".format(clause['ClauseKey'], data['data']['CreateClause']['Code'])) 